const FACTS: &'static str = "Rust > Python";

pub fn give_facts() -> &'static str {
    FACTS
}
