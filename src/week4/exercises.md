# Tehtävät

Lue _kirjasta_
[kappale 6.2](https://doc.rust-lang.org/book/ch06-02-match.html).

## `otarustlings`

Viikon tehtävät tehdään `otarustligs` ohjelmaa käyttäen.

Tehtävät ovat kansiossa `week4`.

`otarustlings`in voit päivittää komennolla:

```console
cargo install --force otarustlings
```

[Lue `otarustlings` ohje](../otarustlings.md)

> Tätä varten tarvitse [Rustin työkalut](../week1/introduction.md#rustin-asennus)

