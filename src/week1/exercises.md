# Tehtävät

## Tehtävistä yleensä kurssilla

Kurssilla tehdään tehtäviä tunneilla, mutta niitä varmasti riittää myös tehtäväksi itsenäisesti. Tehtäviä ei ole jaettu tuntitehtäviin ja kotitehtäviin vaan jako riippuu mitä tunnilla saa aikaan.

## 

## `guessing_game`

<https://doc.rust-lang.org/book/ch02-00-guessing-game-tutorial.html>

_Kirjan_ toisessa kappaleessa päästään vauhtiin kirjoittamalla komentorivipeli.

`guessing_game`a ei tarvitse palauttaa.

