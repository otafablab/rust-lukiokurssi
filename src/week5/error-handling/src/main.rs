use error_handling::config_file;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let conf = config_file("config.json")?;
    println!("{}", conf.name);

    Ok(())
}
