use serde::Deserialize;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::Path;
use std::path::PathBuf;
use std::result;

pub type Result<T> = result::Result<T, ConfigError>;

fn read_file(path: impl AsRef<Path>) -> result::Result<Vec<u8>, std::io::Error> {
    let mut f = File::open(path.as_ref())?;
    let mut buf = Vec::new();
    f.read_to_end(&mut buf)?;
    Ok(buf)
}

#[derive(Deserialize)]
pub struct Config {
    pub name: String,
}

#[derive(Debug)]
pub enum ConfigErrorType {
    FileError(std::io::Error),
    DeserializeError(serde_json::Error),
}

#[derive(Debug)]
pub struct ConfigError {
    pub path: PathBuf,
    pub err: ConfigErrorType,
}

pub fn config_file(path: impl AsRef<Path>) -> Result<Config> {
    let path = path.as_ref();
    let construct_config_error = |err| ConfigError {
        path: path.to_path_buf(),
        err,
    };

    let buf = read_file(path) //  result::Result<Vec<u8>, std::io::Error>
        .map_err(|e| construct_config_error(e.into()))?; // result::Result<Vec<u8>, ConfigErrorType>
    let conf = serde_json::from_slice(&buf) // kana
        .map_err(|e| construct_config_error(e.into()))?;
    Ok(conf)
}

impl From<serde_json::Error> for ConfigErrorType {
    fn from(e: serde_json::Error) -> Self {
        Self::DeserializeError(e)
    }
}

impl From<io::Error> for ConfigErrorType {
    fn from(e: io::Error) -> Self {
        Self::FileError(e)
    }
}

impl std::fmt::Display for ConfigError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self.err {
            ConfigErrorType::FileError(e) => {
                write!(f, "could not open file {:?}: {:?}", self.path, e)
            }
            ConfigErrorType::DeserializeError(e) => {
                write!(f, "could not deserialize file {:?}: {:?}", self.path, e)
            }
        }
    }
}

impl std::error::Error for ConfigError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(match &self.err {
            ConfigErrorType::FileError(f) => f,
            ConfigErrorType::DeserializeError(d) => d,
        })
    }
}
