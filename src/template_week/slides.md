
<!-- .slide: data-background="hsl(210, 25%, 8%)" -->

# TODO Title

---

## Tavut muistissa

<datum class="spaced">
    <name><code>u8</code>, <code>i8</code></name>
    <visual class="bytes">
        <byte><code></code></byte>
    </visual>
</datum>

---

## `TODO`

<div class="ferris-container">
<img src="https://doc.rust-lang.org/book/img/ferris/does_not_compile.svg" style=" top: 0; left: 10em" class="ferris">
</div>

```rust ,compile_fail
let guess = String::new();

std::io::stdin()
    .read_line(&mut guess)
    .expect("Failed to read line");
```

---

```rustc
error[E0596]: cannot borrow `guess` as mutable, as it is not declared as mutable
 --> a.rs:5:20
  |
2 |     let guess = String::new();
  |         ----- help: consider changing this to be mutable: `mut guess`
...
5 |         .read_line(&mut guess)
  |                    ^^^^^^^^^^ cannot borrow as mutable

error: aborting due to previous error
```

---

## Lopuksi

<div class="container"><div class="col-top">

- Muistakaa tutustua [lisämateriaaliin](../extra.html)
  - Huijauslehdet ovat käteviä
  - `explaine.rs`
- Lukekaa _kirjasta_ kappale 4.1
- Etsikää lisää tietoa ja kysykää apua

</div><div class="col-top">

![idiomatic rust](../week3/cheatsrs_idiomatic.png)

</div></div>
