# Yhteenveto

Tällä viikolla opittiin muistinhallinnan yleisiä sekä Rust-spesifejä
yksityiskohtia.

Uusia asioita olivat:

- eri tyyppien esitys muistissa
- pino (stack) ja keko (heap)
- `Vec<T>`-tietueen yksityiskohdat
- referenssit `&` ja osoittimet `*const`
- taulukot `[T, n]` ja slicet `[T]`
- lainaamisen säännöt

Vanhoja asioita joita kerrattiin olivat:

- omistajuus
- skooppi
- tyypit
- `Vec<T>`-tietue
- Rustin erilaisuus muihin kieliin nähden

## Viikkopalaute

Joka viikolla voi ja kannattaa täyttää palautelomake, jonka avulla
opettajat voi päätellä mitä pitäisi opettaa enemmän

[Anna palautetta miten sinulla meni tämän viikon aiheet](https://docs.google.com/forms/d/e/1FAIpQLSeL6KfW4W68onRYFVfrHn1vJzIxGkOejBWK62qds78RrE9Izw/viewform?usp=sf_link)
