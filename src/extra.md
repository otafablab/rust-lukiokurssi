# Lisämateriaali

## Virallinen Rust-kirja

<https://doc.rust-lang.org/book/>

_The Rust Programming Language_, tunnetaan myös nimellä _The Book_, on
Steve Klabnikin ja Carol Nicholsin kirjoittama Rustin virallinen
opetusmateriaali, jota käytetään tällä kurssilla ekstensiivisesti.

> Kirjassa lukee, että sinun tulisi käyttää editiota 2018, mutta
> editio 2021 toimii yhtä hyvin

### Operaattorien ja symbolien syntaksi

<https://doc.rust-lang.org/book/appendix-02-operators.html>

Jos huomaat lukiessasi tuntemattomia symboleita, kuten `&` tai `@`,
tältä sivulta löydät niiden nimet.

> Kannattaa myös kokeilla interaktiivista [`explaine.rs`](#explainers)
> syntaksinselitysjärjestelmää.

## Rustia esimerkeillä

<https://doc.rust-lang.org/stable/rust-by-example/>

_Rust By Example_ käy _kirjan_ aiheita läpi
enemmän-koodia-vähemmän-selitystä ajatuksella.

RBE:n kappaleita kannattaakin silmäillä ennen kirjan kappaleita, jotta
saa paremman yleiskäsityksen Rustista.

## Rustlings-harjoitukset

<https://github.com/rust-lang/rustlings/>

Rustlings on kokoelma lyhyitä Rust-tehtäviä, joissa keskitytään
enemmän toistoon ja tekniikkaan kuin ongelmanratkaisuun. Rustlingsit
on suunniteltu tehtäväksi _kirjan_ lukemisen myötä, ja niihin ei aina
löydy yksiselitteistä ratkaisua.

Tehtäviä on noin 80 ja niiden tekemiseen menee vähintään monta päivää,
jopa kokeneella koodarilla.

> Osa Rustlings-harjoituksista tulee vastaan kurssilla

## Keittokirja

<https://rust-lang-nursery.github.io/rust-cookbook/>

Keittokirjasta löytyy hyödyllisiä koodinpätkiä.

## Rustin verkkosivut

<https://www.rust-lang.org/learn>

## Huijauslehdet

### [`cheats.rs`](https://cheats.rs/)

Hyvin yksityiskohtainen ja yleiskäyttöinen cheatsheet.

### [Programming-Idioms huijauslehti](https://www.programming-idioms.org/cheatsheet/Rust)

Inferiorimpi huijauslehti.

### [Container cheat sheet](./container_cheat_sheet.pdf)

Visualisaatio Rustin säiliöiden muistirakenteesta.

### [Memory Container Cheat-sheet](https://github.com/usagi/rust-memory-container-cs)

Apu oikean säiliön valitsemiseen.

## [`explaine.rs`](https://jrvidal.github.io/explaine.rs/)

Interaktiivinen syntaksin selitys.

## Suunnittelumalleja

<https://rust-unofficial.github.io/patterns/patterns/index.html>

Geneerisiä ja uudelleenkäytettäviä idiomaattisia _ratkaisumalleja_ ongelmiin Rustissa. Suunnittelumallit auttavat kirjoittamaan luettavaa ja ylläpidettävää idiomaattista koodia, joka toimii.

[Lue lisää suunnittelumalleista](https://en.wikipedia.org/wiki/Software_design_pattern)

## Vielä lisää idiomaattista chokoa

- <https://github.com/ferrous-systems/elements-of-rust>
- <https://www.possiblerust.com/>
- <https://rust-lang.github.io/api-guidelines/>
- <https://nnethercote.github.io/perf-book/introduction.html>
- <https://github.com/rust-lang/rust-clippy>
- <https://rust-unofficial.github.io/patterns/idioms/index.html>
- Tämän listan lähde: <https://github.com/mre/idiomatic-rust>

## [`rust.godbolt.org`](https://rust.godbolt.org/)

Haluaisitko mieluummin lukea assemblyä tai _Mid-level Intermediate
Representation_ koodia?

## Rustonomicon, Rustin "pimeän taiteen" kirja

<https://doc.rust-lang.org/nomicon/index.html>

Rustonomicon on tarkoitettu ohjelmoijille, jotka ovat lukeneet
_kirjan_ ja etsivät syvempää ymmärrystä omistajuudesta ja
muistinhallinnasta Rustissa.

## Muita tietolähteitä

- [This week in Rust](https://this-week-in-rust.org/)
- [Rust user forum](https://users.rust-lang.org/)
- [Rust internals forum](https://internals.rust-lang.org/)
- [Ferrous systems blog](https://ferrous-systems.com/blog/)

## Rustilla kehitettyjä ohjelmia

|                                   Ohjelma | Kuvaus                                                                                           |
|------------------------------------------:|--------------------------------------------------------------------------------------------------|
|        [alacritty](https://alacritty.org) | GPU-kiihdytetty pääte-emulaattori                                                                |
|     [bat](https://github.com/sharkdp/bat) | [cat(1)](https://linux.die.net/man/1/cat)-klooni, jossa on syntaksin korostus ja git-integraatio |
|       [exa](https://github.com/ogham/exa) | Parempi [ls(1)](https://linux.die.net/man/1/ls)                                                  |
| [rust](https://github.com/rust-lang/rust) | Rustin kääntäjä `rustc` ja standardikirjasto ovat kirjoitettu rustilla                           |

## Roopen UB esityksen diat 14.11.2022

[PDF](./UB.pdf)
