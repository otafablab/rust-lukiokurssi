// This script should be loaded in mdBook. Adds a presentation button to the top bar

(function init() {
    const rightButtons = document.querySelectorAll('.right-buttons')[0];
    if (!rightButtons) {
        throw new Error("Unable to locate right-buttons")
    }

    const /* mut */ openPresentationLink = document.createElement('a');
    openPresentationLink.href = window.location.pathname.replace(/.html$/, '.reveal.html');
    const /* mut */ openPresentationButton = document.createElement('button');
    openPresentationButton.className = 'icon-button';
    openPresentationButton.innerHTML = '<i class="fa fa-slideshare"></i>';
    openPresentationButton.title = 'Open presentation';
    openPresentationButton.setAttribute('aria-label', openPresentationButton.title);
    
    openPresentationLink.appendChild(openPresentationButton);
    rightButtons.insertBefore(openPresentationLink, rightButtons.firstChild);
})()
